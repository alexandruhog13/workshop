const { query } = require('../data/Conexiune.js');
const fs = require('fs');
const { genereazaEroare } = require('../utils/Erori.js');
const utilitarFisiere = require('../utils/Fisiere.js');

let creazaVectorPoze = (obiectePoze) => {
    const poze = [];

    obiectePoze.forEach((obiectPoza) => {
        try {
            const pozaBytes = fs.readFileSync(utilitarFisiere.calculeazaCalePoze(obiectPoza.nume));
            poze.push({
                id: obiectPoza.id,
                nume: obiectPoza.nume,
                likes: obiectPoza.likes,
                likeFromMe: obiectPoza.like_from_me,
                poza: Buffer.from(pozaBytes).toString('base64')
            });
        } catch (err) {
            console.log(err);
            throw genereazaEroare('Oups, ceva rau s-a intamplat', 500);
        }
    });

    return poze;
}

module.exports.postatDeUtilizator = async (numePoza, idUtilizator) => {
    try {
        await query('INSERT INTO poze (nume, id_utilizator) VALUES ($1, $2) RETURNING id', [numePoza, idUtilizator]);
        return {
            message: `Poza ${numePoza} adaugata cu succes!`,
        }
    } catch (err) {
        console.log(err);
        throw genereazaEroare('Nu s-a putut adauga poza!', 500);
    }
};

module.exports.likeDeLaUtilizator = async (idPoza, idUtilizator) => {
    try {
        let result = await query('SELECT * FROM likes WHERE id_poza = $1 AND id_utilizator = $2', [idPoza, idUtilizator]);
        if (result.rows.length > 0) {
            result = await query('UPDATE poze SET likes = likes - 1 WHERE id = $1 RETURNING *', [idPoza]);
            const numePoza = result.rows[0].nume;
            await query('DELETE FROM likes WHERE id_utilizator = $1 AND id_poza = $2', [idUtilizator, idPoza]);
            return {
                message: `Poza ${numePoza} a fost dislike-uita cu succes`
            }
        } else {
            result = await query('UPDATE poze SET likes = likes + 1 WHERE id = $1 RETURNING *', [idPoza]);
            const numePoza = result.rows[0].nume;
            await query('INSERT INTO likes (id_utilizator, id_poza) VALUES ($1, $2)', [idUtilizator, idPoza]);
            return {
                message: `Poza ${numePoza} a fost like-uita cu succes`
            }
        }

    } catch (err) {
        console.log(err);
        throw genereazaEroare('Nu s-a putut aprecia poza!', 500);
    }
};

module.exports.pozeleMeleLikeuite = async (idUtilizator, pageSize, pageNo) => {
    const limit = pageSize;
    const offset = limit * (pageNo - 1);

    try {
        const result = await query(`SELECT p.id, p.nume, p.likes FROM likes l 
                                    JOIN poze p ON (p.id = l.id_poza)
                                    WHERE l.id_utilizator = $3
                                    LIMIT $1 
                                    OFFSET $2`, [limit, offset, idUtilizator]);
        const poze = creazaVectorPoze(result.rows);

        return {
            message: 'Pozele',
            data: {
                poze: poze
            }
        }
    } catch (err) {
        console.log(err);
        throw genereazaEroare('Nu s-au putut returna pozele!', 500);
    }
}

module.exports.pozelePuseDeMine = async (idUtilizator, pageSize, pageNo) => {
    const limit = pageSize;
    const offset = limit * (pageNo - 1);

    try {
        const result = await query(`SELECT p.id, p.nume, p.likes,
                                        (SELECT COUNT(l.id_utilizator) as like_from_me 
                                        FROM likes l 
                                        WHERE l.id_utilizator = $3 AND l.id_poza = p.id)
                                    FROM poze p
                                    WHERE p.id_utilizator = $3
                                    LIMIT $1 
                                    OFFSET $2`, [limit, offset, idUtilizator]);
        const poze = creazaVectorPoze(result.rows);

        return {
            message: 'Pozele',
            data: {
                poze: poze
            }
        }
    } catch (err) {
        console.log(err);
        throw genereazaEroare('Nu s-au putut returna pozele!', 500);
    }
}

module.exports.toatePozele = async (idUtilizator, pageSize, pageNo) => {
    const limit = pageSize;
    const offset = limit * (pageNo - 1);
    try {
        const result = await query(`SELECT p.id, p.nume, p.likes,
                                        (SELECT COUNT(l.id_utilizator) as like_from_me 
                                        FROM likes l 
                                        WHERE l.id_utilizator = $3 AND l.id_poza = p.id)
                                    FROM poze p
                                    LIMIT $1 
                                    OFFSET $2`, [limit, offset, idUtilizator])
        const poze = creazaVectorPoze(result.rows);
        return {
            message: 'Pozele',
            data: {
                poze: poze
            }
        }
    } catch (err) {
        console.log(err);
        throw genereazaEroare('Nu s-au putut returna pozele!', 500);
    }
}