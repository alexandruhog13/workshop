const CodificatorParola = require('../security/CodificatorParola.js');
const {query} = require('../data/Conexiune.js');
const {genereazaEroare} = require('../utils/Erori.js');
const ProviderJWT = require("../security/ProviderJWT.js");

module.exports.inregistrare = async (email, password) => {
    const parolaCriptata = await CodificatorParola.cripteaza(password);
    try {
        await query('INSERT INTO utilizatori (email, password) VALUES ($1, $2)', [email, parolaCriptata])
        return {
            message: `Utilizator ${email} inregistrat cu succes!`,
        }
    } catch (err) {
        console.log(err);
        if (err.code === '23505') {
            throw genereazaEroare('Utilizatorul deja exista!', 500);
        }
        throw genereazaEroare('Utilizatorul nu s-a putut crea!', 500);
    }
};

module.exports.autentificare = async (email, password) => {
    try {
        const result = await query('SELECT id, password FROM utilizatori WHERE email = $1', [email]);
        if (result.rows.length === 1) {
            const utilizator = result.rows[0];        
            if (CodificatorParola.compara(password, utilizator.password)) {
                const token = await ProviderJWT.genereazaToken({
                    id: utilizator.id,
                    email: email,
                });
                return {
                    message: 'Autentificare cu succes!',
                    data: {
                        token: token
                    }
                };
            } else {
                throw genereazaEroare('Email sau parola gresite!', 401);
            }
        } else {
            throw genereazaEroare('Utilizatorul nu este inregistrat!', 404);
        }
    } catch (err) {
        console.log(err);
        if (err.code === 401 || err.code === 404) {
            throw err;
        }
        throw genereazaEroare('Oups, ceva rau s-a intamplat!', 500);
    };
};