const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
require('dotenv').config();
require('express-async-errors');

const app = express();
app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(morgan('tiny'));

app.use(express.static('public'))
const {
    bindRoutes
} = require('./routes.js');
bindRoutes(app);
app.listen(process.env.PORT, (err) => {
    if (err) {
        console.error(err);
    } else {
        console.log(`app is listening on port ${process.env.PORT}`);
    }
});
