module.exports.genereazaEroare = (message, code) => {
    let error = new Error(message);
    error.code = code;
    return error;
}