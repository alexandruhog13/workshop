const ruteAutentificare = require('./controllers/Autentificare.js');
const rutePoze = require('./controllers/Poze.js');

module.exports.bindRoutes = (app) => {
    app.use(`/api/${ruteAutentificare.cale}`, ruteAutentificare.router);
    app.use(`/api/${rutePoze.cale}`, rutePoze.router);
    
    app.use((err, req, res, next) => {
        if (!err.code || typeof err.code == "string") {
            err.code = 500;
        }
        res.status(err.code).json({
            error: err.message
        });
        next(err);
    })
};