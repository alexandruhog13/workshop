const Router = require('express').Router();
const ServiciuAutentificare = require('../services/ServiciuAutentificare.js');
const HttpStatus = require("http-status-codes");

Router.post('/inregistrare', async (req, res, next) => {
    console.log('ola');
    const data = await ServiciuAutentificare.inregistrare(req.body.email, req.body.password);
    res.status(HttpStatus.CREATED).json(data);
});

Router.post('/autentificare', async (req, res, next) => {
    const data = await ServiciuAutentificare.autentificare(req.body.email, req.body.password);
    res.status(HttpStatus.OK).json(data);
});

module.exports = {
    cale: 'auth',
    router: Router
}