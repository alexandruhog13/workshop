const Router = require('express').Router();
const ServiciuPoze = require('../services/ServiciuPoze.js');
const FiltruJWT = require('../security/FiltruJWT.js');
const multer = require('multer');
const HttpStatus = require('http-status-codes');
const sharp = require('sharp');
const fileType = require('file-type');

const utilitarFisiere = require('../utils/Fisiere.js');
const { genereazaEroare } = require('../utils/Erori.js');

const storage = multer.memoryStorage();
const upload = multer({
    storage: storage
});

Router.post('/like', FiltruJWT.autorizeazaToken, async (req, res, next) => {
    const data = await ServiciuPoze.likeDeLaUtilizator(req.body.idPoza, req.state.id);
    res.status(HttpStatus.OK).json(data);
})

Router.post('/', FiltruJWT.autorizeazaToken, upload.single('poza'), async (req, res, next) => {

    let mime = fileType(req.file.buffer);
    if (!mime || !utilitarFisiere.extensiiAcceptate.includes(mime.ext)){
        throw genereazaEroare('Doar jpg, jpeg sau png!', HttpStatus.BAD_REQUEST);
    }
    let fileName = `${Date.now()}.${mime.ext}`;
    const fullPath = utilitarFisiere.calculeazaCalePoze(fileName);
    try {
        await sharp(req.file.buffer)
        .resize(1080, 1350)
        .toFile(fullPath);
    } catch (err) {
        console.log(err);
        throw genereazaEroare('Oups, something bad happened!', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    const data = await ServiciuPoze.postatDeUtilizator(fileName, req.state.id);
    res.status(HttpStatus.OK).json(data);

});

Router.get('/like', FiltruJWT.autorizeazaToken, async (req, res, next) => {
    const data = await ServiciuPoze.pozeleMeleLikeuite(req.state.id, req.query.pageSize, req.query.pageNo);
    res.status(HttpStatus.OK).json(data);
})

Router.get('/colectiaMea', FiltruJWT.autorizeazaToken, async (req, res, next) => {
    const data = await ServiciuPoze.pozelePuseDeMine(req.state.id, req.query.pageSize, req.query.pageNo);
    res.status(HttpStatus.OK).json(data);
})

Router.get('/', FiltruJWT.autorizeazaToken, async (req, res, next) => {
    const data = await ServiciuPoze.toatePozele(req.state.id, req.query.pageSize, req.query.pageNo);
    res.status(HttpStatus.OK).json(data);
})
module.exports = {
    cale: 'poze',
    router: Router
}