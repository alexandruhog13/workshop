const validator = require("validator");
const HttpStatus = require("http-status-codes");
const ProviderJWT = require("./ProviderJWT.js");
const {genereazaEroare} = require("../utils/Erori.js");

module.exports.autorizeazaToken = async (req, res, next) => {
  if (!req.headers.authorization) {
    throw genereazaEroare("Neautorizat!", HttpStatus.FORBIDDEN);
  }
  const token = req.headers.authorization.split(" ")[1];
  if (!validator.isJWT(token)) {
    throw genereazaEroare("Tokenul tau nu este valid!", HttpStatus.BAD_REQUEST);
  }
  const decoded = await ProviderJWT.verificaToken(token);
  if (!decoded) {
    throw genereazaEroare("Neautorizat!", HttpStatus.FORBIDDEN);
  }
  req.state = decoded;
  next();
};